var express = require("express");
var app = express();
app.use(express.json());
var cfg = require("./config.js");
var jwt = require("jsonwebtoken");
var passport = require("passport");
var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;
var params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};
passport.initialize();
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, , authorization"
  );
  res.setHeader("Access-Control-Expose-Headers", "X-Auth-Token");
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});
const { createPool } = require("mysql");
const pool = createPool({
  host: "localhost",
  user: "root",
  password: "root",
  database: "institute",
  connectionLimit: 10,
  port: 2410,
});
//for registering new user
app.post("/register", function (req, res) {
  pool.query(`select * from persons`, (err, result) => {
    let email = req.body.email;
    let obj = result.find((item) => item.email === email);
    if (obj === undefined) {
      pool.query(
        `insert into persons (name,email,password,role) values ('${req.body.name}','${req.body.email}','${req.body.password}','${req.body.role}')`,
        (err1, result1) => {
          if (err1) throw err1;
          console.log("Inserted");
        }
      );
      let person = {
        name: req.body.name,
        role: req.body.role,
        email: req.body.email,
      };
      res.send(person);
    } else res.status(500).send("Email already present");
  });
});
//for Login
const jwtExpirySeconds = 300;
app.post("/login", function (req, res, payload) {
  if (req.body.email && req.body.password) {
    var email = req.body.email;
    var password = req.body.password;
    pool.query(`select * from persons`, (err, result, fields) => {
      if (err) {
        return console.log(err);
      }
      var user = result.find(
        (obj) => obj.email === email && obj.password === password
      );
      console.log("397", user);
      if (user) {
        var payload = {
          email: user.email,
        };
        var token = jwt.sign(payload, cfg.jwtSecret, {
          algorithm: "HS256",
          expiresIn: jwtExpirySeconds,
        });
        res.setHeader("X-Auth-Token", token);
        let json = {};
        json.id = user.id;
        json.name = user.name;
        json.email = user.email;
        json.role = user.role;
        json.token = token;
        json.success = true;
        res.send(json);
      } else {
        res.status(401).send("Login Unsuccessful");
      }
    });
  } else {
    res.status(401).send("Login Unsuccessful");
  }
});
var strategy1 = new Strategy(params, function (payload, done) {
  console.log(payload);
  pool.query(
    `select * from persons where email='${payload.email}'`,
    (err, result) => {
      if (err) return console.log(err);
      if (result) {
        return done(null, {
          result,
        });
      } else {
        return done(new Error("Person not Found"), null);
      }
    }
  );
});
passport.use("local.one", strategy1);
app.get(
  "/getPersons",
  passport.authenticate("local.one", { session: false }),
  (req, res) => {
    let roleStr = req.query.role;
    let roleArr = [];
    let pageNum = req.query.page ? +req.query.page : 1;
    let pageSize = 5;
    let startIndex = (pageNum - 1) * pageSize;
    if (roleStr) {
      roleArr = roleStr.split(",");
    }
    pool.query("select id,name,email,role from persons", (err, result) => {
      console.log(roleStr);
      let copyArr = result;
      if (roleArr.length !== 0) {
        copyArr = copyArr.filter((obj) =>
          roleArr.find((obj1) => obj1 === obj.role)
        );
      }
      if (roleStr === false) {
        let pageArr = [...result];
        tempArr = pageArr.splice(startIndex, pageSize);
        let totalNum = result.length;
        let totalItems = tempArr.length;
        let json = {};
        json.persons = tempArr;
        json.pageNum = pageNum;
        json.pageSize = pageSize;
        json.totalItems = totalItems;
        json.totalNum = totalNum;
        res.send(json);
      } else {
        let totalNum = copyArr.length;
        let pageArr = [...copyArr];
        tempArr = pageArr.splice(startIndex, pageSize);
        let totalItems = tempArr.length;
        let json = {};
        json.persons = tempArr;
        json.pageNum = pageNum;
        json.pageSize = pageSize;
        json.totalItems = totalItems;
        json.totalNum = totalNum;
        res.send(json);
      }
    });
  }
);
//for fetching all courses
app.get(
  "/getCourses",
  passport.authenticate("local.one", { session: false }),
  function (req, res) {
    pool.query(`select * from courses`, (err, result) => {
      if (err) throw err;
      console.log(result);
      res.send(result);
    });
  }
);
//for fetching all persons without pagination
app.get(
  "/allPersons",
  passport.authenticate("local.one", { session: false }),
  (req, res) => {
    pool.query(`select id,name,email,role from persons`, (err, result) => {
      if (err) throw err;
      res.send(result);
    });
  }
);
//for add Course
app.post("/addCourse", (req, res) => {
  let title = req.body.title;
  let faculty = req.body.faculty;
  pool.query(
    `insert into courses (title,facultyId) values ('${title}',${faculty})`,
    (err, result) => {
      if (err) throw err;
      console.log("Course Inserted");
    }
  );
  res.send(req.body);
});
// fetch faculty Courses
app.get(
  "/getFacultyCourse/:facultyName",
  passport.authenticate("local.one", { session: false }),
  function (req, res) {
    let name = req.params.facultyName;
    console.log(name);
    pool.query(
      `select cId, title, name from courses inner join persons on courses.facultyId = persons.id where name='${name}'`,
      (err, result) => {
        if (err) throw err;
        res.send(result);
      }
    );
  }
);
// fetch Faculty Lectures
app.get(
  "/getFacultyLecture/:title",
  passport.authenticate("local.one", { session: false }),
  function (req, res) {
    let title = req.params.title;
    pool.query(
      `select lId, title, topic, date from lectures inner join courses on lectures.courseId=courses.cId where title='${title}'`,
      (err, result) => {
        if (err) throw err;
        res.send(result);
      }
    );
  }
);
app.post("/addLecture", function (req, res) {
  let course = req.body.course;
  let topic = req.body.topic;
  let date = req.body.date;
  pool.query(
    `select * from courses where title='${course}'`,
    (err1, result1) => {
      if (err1) throw err1;
      console.log(result1);
      let id = result1[0].cId;
      console.log(id);
      pool.query(
        `insert into lectures (courseId,topic,date) values (${id},'${topic}','${date}')`,
        (err, result) => {
          if (err) throw err;
          console.log("Inserted");
        }
      );
    }
  );
  res.send(req.body);
});
app.get(
  "/getLecture/:id",
  passport.authenticate("local.one", { session: false }),
  (req, res) => {
    let id = req.params.id;
    pool.query(`select * from lectures where lId =${id}`, (err, result) => {
      if (err) throw err;
      res.send(result);
    });
  }
);
//update Attendence
app.put("/updateAttendence/:id", (req, res) => {
  let id = req.params.id;
  let attendence = req.body.attendence;
  pool.query(
    `update lectures set attendence='${attendence}' where lId = ${id}`,
    (err, result) => {
      if (err) throw err;
      console.log("Updated");
    }
  );
  res.send(req.body);
});
//fetching Student Course
app.get(
  "/getStudentCourse/:id",
  passport.authenticate("local.one", { session: false }),
  function (req, res) {
    let id = req.params.id;
    console.log(id);
    pool.query(`select * from courses`, (err, result) => {
      if (err) throw err;
      let cor = [];
      result.filter((obj) => {
        let arr = obj.students ? obj.students.split(",") : [];
        arr.find((obj1) => {
          if (obj1 === id) return cor.push(obj);
        });
      });
      res.send(cor);
    });
  }
);
//enroll student into Course
app.put("/enroll/:name", (req, res) => {
  let name = req.params.name;
  let id = req.body.id;
  console.log(id);
  pool.query(`select * from courses where title = '${name}'`, (err, result) => {
    if (err) throw err;
    let arr = result[0].students ? result[0].students.split(",") : [];
    arr.push(id);
    let students = arr.join(",");
    pool.query(
      `update courses set students = '${students}' where title='${name}'`,
      (err, result) => {
        if (err) throw err;
        console.log("updated");
      }
    );
  });
  res.send("Assigned");
});
// fetching all Lectures of Students
app.get("/getStudentLectures/:id", function (req, res) {
  let id = req.params.id;
  let courseStr = req.query.course;
  let courseArr = [];
  if (courseStr) {
    courseArr = courseStr.split(",");
  }
  console.log(id);
  pool.query(`select * from courses`, (err, result) => {
    if (err) throw err;
    let course = [];
    result.filter((obj) => {
      let arr = obj.students ? obj.students.split(",") : [];
      arr.find((obj1) => {
        if (obj1 === id) course.push(obj.title);
      });
    });
    pool.query(
      `select lId,title,topic,date from lectures inner join courses on lectures.courseId = courses.cId`,
      (err1, result1) => {
        if (err1) throw err;
        var lectures = result1.filter((obj) =>
          course.find((obj1) => obj1 === obj.title) ? obj : ""
        );
        let copyArr = lectures;
        if (courseArr.length !== 0) {
          copyArr = copyArr.filter((obj) =>
            courseArr.find((obj1) => obj1 === obj.title)
          );
        }
        if (courseStr === false) {
          res.send(lectures);
        } else {
          res.send(copyArr);
        }
      }
    );
  });
});
//fetch all lectures
app.get("/allLectures", (req, res) => {
  pool.query(`select * from lectures`, (err, result) => {
    if (err) throw err;
    res.send(result);
  });
});

const port = 2450;
app.listen(port, () => console.log(`Listening on ${port}!`));
