import React, { Component } from "react";
import http from "./httpService";
class ShowCourses extends Component {
  state = {
    data: [],
    persons: [],
  };
  async fetchdata() {
    let response = await http.get(`/getCourses`);
    let persons = await http.get(`/allPersons`);
    let { data } = response;
    this.setState({ data: data, persons: persons.data });
    console.log(response);
    console.log(persons);
  }
  componentDidMount() {
    this.fetchdata();
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) this.fetchdata();
  }
  render() {
    let { data = [] } = this.state;
    return (
      <div className="container">
        <h4 className="mt-4 mb-0">All Courses</h4>
        <div className="row bg-dark text-light text-center">
          <div className="col-2">Id</div>
          <div className="col-2">Title</div>
          <div className="col-2">Faculty</div>
          <div className="col-6">Students</div>
        </div>
        {data.map((st, index) => (
          <div className="row bg-light text-dark border text-center">
            <div className="col-2">{st.cId}</div>
            <div className="col-2">{st.title}</div>
            <div className="col-2">{this.makeFaculty(st.facultyId)}</div>
            <div className="col-6">
              {st.students ? this.makeStudents(st.students) : ""}
            </div>
          </div>
        ))}
      </div>
    );
  }
  makeStudents = (students) => {
    let { persons = [] } = this.state;
    let studArr = students.split(",");
    let name = studArr.map((st) => {
      let find = persons.find((obj) => obj.id === +st);
      return find.name;
    });
    return name.join(", ");
  };
  makeFaculty = (id) => {
    let { persons = [] } = this.state;
    let find = persons.find((obj) => obj.id === id);
    if (find) {
      return find.name;
    }
  };
}
export default ShowCourses;
