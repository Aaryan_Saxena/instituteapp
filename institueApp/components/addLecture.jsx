import React, { Component } from "react";
import http from "./httpService";
class AddLecture extends Component {
  state = {
    lecture: {
      course: this.props.match.params.title,
      topic: "",
      date: "",
    },
    courses: [],
    errors: {},
  };
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.lecture[input.name] = input.value;
    this.handleValidate(e);
    this.setState(s1);
  };
  isvalid = (errors) => {
    let keys = Object.keys(errors);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  isFormValid = () => {
    let errors = this.validateAll();
    return this.isvalid(errors);
  };
  validateAll = () => {
    let { topic, date } = this.state.lecture;
    let errors = {};
    errors.date = this.validateDate(date);
    errors.topic = this.validateTopic(topic);
    return errors;
  };
  handleValidate = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    switch (input.name) {
      case "date":
        s1.errors.date = this.validateDate(input.value);
        break;
      case "topic":
        s1.errors.topic = this.validateTopic(input.value);
        break;
      default:
        break;
    }
    this.setState(s1);
  };
  validateDate = (date) => (!date ? "Date is Required" : "");
  validateTopic = (topic) => (!topic ? "Topic is Required" : "");

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.lecture);
    this.postData("/addLecture", this.state.lecture);
  };
  async postData(url, obj) {
    try {
      let response = await http.post(url, obj);
      if (response) {
        alert("Lecture Created Succesfully");
        window.location = "/courseAssigned";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        this.setState({ msg: ex.response.data });
      }
    }
  }
  render() {
    let { lecture, errors = {}, courses = [] } = this.state;
    let { date, topic } = lecture;
    return (
      <div className="container">
        <h5 className="mt-3">Schedule a Lecture</h5>
        {this.makeTextField(
          "date",
          date,
          "date",
          "Enter Date dd-mm-yyyy",
          errors.date
        )}
        {this.makeTextField(
          "topic",
          topic,
          "Topic",
          "Enter Class Topic",
          errors.topic
        )}
        <button
          className="btn btn-primary mt-2"
          disabled={!this.isFormValid()}
          onClick={this.handleSubmit}
        >
          Schedule Lecture
        </button>
      </div>
    );
  }

  makeTextField = (name, value, label, placeholder, errors = "") => {
    let edit = this.state.edit;
    return (
      <div className="row">
        <div className="col-12">
          <label>
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-12">
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name={name}
              value={value}
              placeholder={placeholder}
              onChange={this.handleChange}
            />
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </div>
    );
  };
}
export default AddLecture;
