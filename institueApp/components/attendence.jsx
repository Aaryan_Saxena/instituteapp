import React, { Component } from "react";
import httpService from "./httpService";
class Attendence extends Component {
  state = { persons: [], attendence: [] };
  async componentDidMount() {
    let { id } = this.props.match.params;
    console.log(id);
    let persons = await httpService.get(`/allPersons`);
    let arr = [];
    persons.data.map((obj) => {
      if (obj.role === "Student") {
        let json = {};
        json.id = obj.id;
        json.name = obj.name;
        arr.push(json);
      }
    });
    let response = await httpService.get(`/getLecture/${id}`);
    console.log(response);
    let att = response.data[0].attendence
      ? response.data[0].attendence.split(",")
      : [];
    this.setState({
      attendence: att,
      persons: arr,
    });
  }
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1[input.name] = this.updateCBs(s1[input.name], input.checked, input.value);
    this.setState(s1);
  };
  updateCBs = (arr, checked, value) => {
    if (checked) arr.push(value);
    else {
      let index = arr.findIndex((val) => val === value);
      if (index >= 0) arr.splice(index, 1);
    }
    return arr;
  };
  async postAttendence(url, obj) {
    let response = await httpService.put(url, obj);
    console.log(response);
    window.location = "/courseAssigned";
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let json = {};
    json.attendence = this.state.attendence.join(",");
    this.postAttendence(
      `/updateAttendence/${this.props.match.params.id}`,
      json
    );
  };
  render() {
    let { attendence = [], persons = [] } = this.state;
    return (
      <div className="container">
        <h3>Mark Attendence</h3>
        {this.makeCBs(persons, attendence, "attendence", "")}
        <button className="btn btn-primary mt-2" onClick={this.handleSubmit}>
          Close Attendence
        </button>
      </div>
    );
  }
  makeCBs = (arr, values, name, label) => {
    return (
      <div className="row">
        {arr.map((opt, index) => (
          <div className="col-12" key={index}>
            <div className="form-check">
              <input
                type="checkbox"
                className="form-check-input"
                value={opt.id}
                name={name}
                checked={values.find((val) => +val === opt.id)}
                onChange={this.handleChange}
              />
              <label className="form-check-label">{opt.name}</label>
            </div>
          </div>
        ))}
      </div>
    );
  };
}
export default Attendence;
