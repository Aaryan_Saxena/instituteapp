import axios from "axios";
const baseURL = "http://localhost:2450";
function get(url) {
  let str = localStorage.getItem("user");
  let user = str ? JSON.parse(str) : null;
  let token = user.token;
  return axios.get(baseURL + url, {
    headers: { Authorization: `bearer ${token}` },
  });
}
function post(url, obj) {
  return axios.post(baseURL + url, obj);
}
function put(url, obj) {
  return axios.put(baseURL + url, obj);
}
function deleteReq(url) {
  return axios.delete(baseURL + url);
}

export default { get, post, put, deleteReq };
