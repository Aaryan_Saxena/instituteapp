import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import auth from "./authService";
import ShowNavbar from "./showNavbar";
import Login from "./login";
import Logout from "./logout";
import AdminPortal from "./adminPortal";
import RegisterStudent from "./registerStudent";
import ShowPersons from "./showPersons";
import ShowCourses from "./showCourses";
import AddCourse from "./addCourse";
import StudentPortal from "./studentPortal";
import ShowStudentCourse from "./showStudentCourse";
import ShowStudentLectures from "./showStudentLectures";
import FacultyPortal from "./facultyPortal";
import CourseAssigned from "./courseAssigned";
import ScheduledLectures from "./scheduledLectures";
import AddLecture from "./addLecture";
import NotAllowed from "./notAllowed";
import Attendence from "./attendence";
import ShowAttendence from "./showAttendence";
class StudyPortal extends Component {
  render() {
    const user = auth.getUser();
    return (
      <React.Fragment>
        <ShowNavbar user={user} />
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Route
            path="/admin"
            render={(props) =>
              user ? (
                user.role === "Admin" ? (
                  <AdminPortal {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/register"
            render={(props) =>
              user ? (
                user.role === "Admin" ? (
                  <RegisterStudent {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/persons"
            render={(props) =>
              user ? (
                user.role === "Admin" ? (
                  <ShowPersons {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/courses"
            render={(props) =>
              user ? (
                user.role === "Admin" ? (
                  <ShowCourses {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/addCourse"
            render={(props) =>
              user ? (
                user.role === "Admin" ? (
                  <AddCourse {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/student"
            render={(props) =>
              user ? (
                user.role === "Student" ? (
                  <StudentPortal {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/faculty"
            render={(props) =>
              user ? (
                user.role === "Faculty" ? (
                  <FacultyPortal {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/courseStudent"
            render={(props) =>
              user ? (
                user.role === "Student" ? (
                  <ShowStudentCourse {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/courseAssigned"
            render={(props) =>
              user ? (
                user.role === "Faculty" ? (
                  <CourseAssigned {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/scheduledLectures/:title"
            render={(props) =>
              user ? (
                user.role === "Faculty" ? (
                  <ScheduledLectures {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/scheduleLecture/:title"
            render={(props) =>
              user ? (
                user.role === "Faculty" ? (
                  <AddLecture {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/attendence/:id"
            render={(props) =>
              user ? (
                user.role === "Faculty" ? (
                  <Attendence {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/allLectures"
            render={(props) =>
              user ? (
                user.role === "Student" ? (
                  <ShowStudentLectures {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/showAttendence/:lId/:id"
            render={(props) =>
              user ? (
                user.role === "Student" ? (
                  <ShowAttendence {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route path="/notAllowed" component={NotAllowed} />
          <Redirect from="/" to="/login" />
        </Switch>
      </React.Fragment>
    );
  }
}
export default StudyPortal;
