import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./httpService";
class CourseAssigned extends Component {
  state = { course: [] };
  async componentDidMount() {
    let { user } = this.props;
    let response = await http.get(`/getFacultyCourse/${user.name}`);
    console.log(response);
    this.setState({ course: response.data });
  }
  render() {
    let { course = [] } = this.state;
    return (
      <div className="container">
        <h4 className="mt-3">Courses Assigned</h4>
        <div className="row bg-dark text-white text-center">
          <div className="col-3">Course Id</div>
          <div className="col-3">Course Name</div>
          <div className="col-3">Faculty Name</div>
          <div className="col-3"></div>
        </div>
        {course.map((c1) => (
          <div className="row border bg-light text-center">
            <div className="col-3">{c1.cId}</div>
            <div className="col-3">{c1.title}</div>
            <div className="col-3">{c1.name}</div>
            <div className="col-3">
              <Link to={`/scheduledLectures/${c1.title}`}>
                <button className="btn btn-secondary btn-sm my-1">
                  View Lectures
                </button>
              </Link>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
export default CourseAssigned;
