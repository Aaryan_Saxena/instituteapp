import React, { Component } from "react";
import httpService from "./httpService";
class ShowAttendence extends Component {
  state = { lectures: [] };
  async componentDidMount() {
    let response = await httpService.get("/allLectures");
    this.setState({ lectures: response.data });
  }
  render() {
    let { lectures = [] } = this.state;
    let { lId, id } = this.props.match.params;
    if (lectures.length !== 0) {
      let find = lectures.find((obj) => obj.lId === +lId);
      console.log(find);
      if (find) {
        let attendence = find.attendence.split(",");
        console.log(attendence);
        var check = attendence.find((obj) => obj === id);
        console.log(check);
      }
    }
    return (
      <div className="container">
        <h4 className="text-muted text-center mt-5">
          Attendence : {check ? "Present" : "Absent"}
        </h4>
      </div>
    );
  }
}
export default ShowAttendence;
