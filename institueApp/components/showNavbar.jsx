import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Navbar, Form, NavDropdown, Button, Nav } from "react-bootstrap";
class ShowNavbar extends Component {
  render() {
    const { user } = this.props;
    return (
      <React.Fragment>
        <Navbar bg="dark" expand="lg" variant="dark">
          <Navbar.Brand>
            {user
              ? user.role === "Admin"
                ? "Home"
                : user.role === "Student"
                ? "Student Home"
                : "Faculty Home"
              : "Home"}
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              {user && user.role === "Admin" && (
                <Nav.Link>
                  <Link to="/register" className="text-white">
                    Register
                  </Link>
                </Nav.Link>
              )}
              {user && user.role === "Admin" && (
                <Nav.Link>
                  <Link to="/persons" className="text-white">
                    View
                  </Link>
                </Nav.Link>
              )}
              {user && user.role === "Admin" && (
                <NavDropdown title="Courses" id="basic-nav-dropdown">
                  <NavDropdown.Item>
                    <Link to="/addCourse" className="text-dark">
                      Add Course
                    </Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link to="/courses" className="text-dark">
                      View Course
                    </Link>
                  </NavDropdown.Item>
                </NavDropdown>
              )}
              {user && user.role === "Student" && (
                <Nav.Link>
                  <Link to="/courseStudent" className="text-white">
                    Courses
                  </Link>
                </Nav.Link>
              )}
              {user && user.role === "Student" && (
                <Nav.Link>
                  <Link to="/allLectures" className="text-white">
                    All Lectures
                  </Link>
                </Nav.Link>
              )}
              {user && user.role === "Faculty" && (
                <Nav.Link>
                  <Link to="/courseAssigned" className="text-white">
                    Courses
                  </Link>
                </Nav.Link>
              )}
            </Nav>
            <Form inline>
              {user ? (
                <p className="mb-0 mx-3 text-white">Welcome {user.name}</p>
              ) : (
                ""
              )}
              {user ? (
                <Link to="/logout">
                  <Button variant="outline-light">
                    Logout <i className="fas fa-sign-in"></i>
                  </Button>
                </Link>
              ) : (
                <Link to="/login">
                  <Button variant="outline-light">
                    Login <i className="fas fa-sign-in"></i>
                  </Button>
                </Link>
              )}
            </Form>
          </Navbar.Collapse>
        </Navbar>
      </React.Fragment>
    );
  }
}
export default ShowNavbar;
