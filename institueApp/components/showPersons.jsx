import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Table } from "react-bootstrap";
import queryString from "query-string";
import http from "./httpService";
import LeftPanel from "./leftPanel";
class ShowPersons extends Component {
  state = {
    data: {},
    roles: ["Admin", "Student", "Faculty"],
  };
  async fetchdata() {
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    let response = await http.get(`/getPersons?${searchStr}`);
    let { data } = response;
    this.setState({ data: data });
    console.log(response);
  }
  componentDidMount() {
    this.fetchdata();
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) this.fetchdata();
  }
  handleOptionChange = (options) => {
    this.callURL("/persons", options);
  };
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    let { page = "1" } = queryParams;
    let newPage = +page + incr;
    queryParams.page = newPage;
    this.callURL("/persons", queryParams);
  };
  callURL = (url, options) => {
    let searchStr = this.makeSearchString(options);
    this.props.history.push({ pathname: url, search: searchStr });
  };
  makeSearchString = (options) => {
    let { page, role } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "page", page);
    searchStr = this.addToQueryString(searchStr, "role", role);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    let { data = {}, roles = [] } = this.state;
    let {
      persons = [],
      pageNum = "",
      pageSize = "",
      totalItems = "",
      totalNum = "",
    } = data;
    let size = pageSize;
    let page = pageNum;
    let startIndex = (page - 1) * size;
    let endIndex = startIndex + totalItems - 1;
    let queryParams = queryString.parse(this.props.location.search);
    return (
      <div className="container">
        <div className="row">
          <div className="col-3 mt-4">
            <LeftPanel
              options={queryParams}
              roles={roles}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-9">
            <h4 className="mt-4 mb-0">All Persons</h4>
            <p>
              {startIndex + 1} - {endIndex + 1} of {totalNum}
            </p>
            <div className="row bg-dark text-white">
              <div className="col-3">Id</div>
              <div className="col-3">Name</div>
              <div className="col-3">Email</div>
              <div className="col-3">Role</div>
            </div>
            {persons.map((st, index) => (
              <div className="row bg-light border" key={index}>
                <div className="col-3">{st.id}</div>
                <div className="col-3">{st.name}</div>
                <div className="col-3">{st.email}</div>
                <div className="col-3">{st.role}</div>
              </div>
            ))}
            <div className="row">
              <div className="col-2">
                {pageNum > 1 ? (
                  <button
                    className="btn btn-secondary m-1 btn-sm"
                    onClick={() => this.handlePage(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-2 offset-8 text-right">
                {totalNum > endIndex + 1 ? (
                  <button
                    className="btn btn-secondary btn-sm m-1"
                    onClick={() => this.handlePage(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ShowPersons;
