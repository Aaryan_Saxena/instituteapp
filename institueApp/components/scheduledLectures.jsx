import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./httpService";
import AddLecture from "./addLecture";
class ScheduledLectures extends Component {
  state = { lectures: [], view: 0, classObj: {} };
  async componentDidMount() {
    let { title } = this.props.match.params;
    let response = await http.get(`/getFacultyLecture/${title}`);
    console.log(response);
    this.setState({ lectures: response.data });
  }
  handleEdit = (classObj) => {
    this.setState({ classObj: classObj, view: 1 });
  };

  async putData(url, obj) {
    console.log(obj);
    try {
      let response = await http.put(url, obj);
      if (response) {
        alert("Class Updated Succesfully");
        window.location = "/scheduledClasses";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        this.setState({ msg: ex.response.data });
      }
    }
  }
  render() {
    let { lectures = [], view, classObj = {} } = this.state;
    return (
      <div className="container">
        {view === 0 ? (
          <React.Fragment>
            <h4 className="mt-3">All Scheduled lectures</h4>
            <div className="row bg-dark text-white text-center">
              <div className="col-2">Lecture Id</div>
              <div className="col-2">Course</div>
              <div className="col-2">Topic</div>
              <div className="col-2">Date</div>
              <div className="col-4"></div>
            </div>
            {lectures.map((c1) => (
              <div className="row border bg-light text-center">
                <div className="col-2">{c1.lId}</div>
                <div className="col-2">{c1.title}</div>
                <div className="col-2">{c1.topic}</div>
                <div className="col-2">{c1.date}</div>
                <div className="col-4">
                  <Link to={`/attendence/${c1.lId}`}>
                    <button className="btn btn-secondary btn-sm">
                      Mark Attendence
                    </button>
                  </Link>
                </div>
              </div>
            ))}
            <Link to={`/scheduleLecture/${this.props.match.params.title}`}>
              <button className="btn btn-secondary mt-2">
                Add New Lecture
              </button>
            </Link>
          </React.Fragment>
        ) : (
          <AddLecture classObj={classObj} putData={this.putData} />
        )}
      </div>
    );
  }
}
export default ScheduledLectures;
