import React, { Component } from "react";
import http from "./httpService";
class ShowStudentCourse extends Component {
  state = { courses: [], enroll: [] };
  async componentDidMount() {
    let { user } = this.props;
    let courses = await http.get(`/getCourses`);
    let response = await http.get(`/getStudentCourse/${user.id}`);
    console.log(response);
    courses = courses.data.reduce(
      (acc, curr) =>
        acc.find((obj) => obj === curr.title) ? acc : [...acc, curr.title],
      []
    );
    this.setState({ courses: courses, enroll: response.data });
  }
  async handleEnroll(course, id) {
    let json = {};
    json.id = id;
    let response = await http.put(`/enroll/${course}`, json);
    console.log(response);
    window.location = "/courseStudent";
  }
  render() {
    let { courses = [], enroll } = this.state;
    return (
      <div className="container">
        <h4 className="mt-3">Courses Assigned</h4>
        <div className="row bg-dark text-white text-center">
          <div className="col-6">Course</div>
          <div className="col-6">Assigned</div>
        </div>
        {courses.map((c1) => (
          <div className="row border bg-light text-center" key={c1.courseId}>
            <div className="col-6">{c1}</div>
            <div className="col-6">
              {enroll.find((obj) => obj.title === c1) ? (
                ""
              ) : (
                <button
                  className="btn btn-danger btn-sm"
                  onClick={() => this.handleEnroll(c1, this.props.user.id)}
                >
                  Enroll
                </button>
              )}
            </div>
          </div>
        ))}
      </div>
    );
  }
}
export default ShowStudentCourse;
