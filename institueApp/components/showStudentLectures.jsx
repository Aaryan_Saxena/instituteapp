import React, { Component } from "react";
import queryString from "query-string";
import http from "./httpService";
import LeftPanelLectures from "./leftPanellectures";
import { Link } from "react-router-dom";
class ShowStudentCourse extends Component {
  state = { lectures: [], enroll: [] };
  async fetchdata() {
    let { user } = this.props;
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    let response = await http.get(
      `/getStudentLectures/${user.id}?${searchStr}`
    );
    let courses = await http.get(`/getStudentCourse/${user.id}`);
    courses = courses.data.reduce(
      (acc, curr) =>
        acc.find((obj) => obj === curr.title) ? acc : [...acc, curr.title],
      []
    );
    console.log(response);
    this.setState({ lectures: response.data, enroll: courses });
  }
  componentDidMount() {
    this.fetchdata();
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) this.fetchdata();
  }
  handleOptionChange = (options) => {
    this.callURL("/allLectures", options);
  };
  callURL = (url, options) => {
    let searchStr = this.makeSearchString(options);
    this.props.history.push({ pathname: url, search: searchStr });
  };
  makeSearchString = (options) => {
    let { course } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "course", course);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    let { lectures = [], enroll = [] } = this.state;
    let queryParams = queryString.parse(this.props.location.search);
    if (queryParams.course) {
      queryParams = queryParams;
    } else {
      queryParams.course = enroll.join(",");
    }
    return (
      <div className="container">
        <div className="row">
          <div className="col-3">
            <LeftPanelLectures
              options={queryParams}
              courses={enroll}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-9">
            <h4 className="mt-3">All Scheduled lectures</h4>
            <div className="row text-light bg-dark text-center">
              <div className="col-3">Lectures Id</div>
              <div className="col-3">Course Name</div>
              <div className="col-3">Topic</div>
              <div className="col-3">Date</div>
            </div>
            {lectures.map((c1, index) => (
              <div className="row border bg-light text-center" key={index}>
                <div className="col-3">{c1.lId}</div>
                <div className="col-3">{c1.title}</div>
                <div className="col-3">
                  <Link
                    to={`/showAttendence/${c1.lId}/${this.props.user.id}`}
                    className="text-dark"
                  >
                    {c1.topic}{" "}
                  </Link>
                </div>
                <div className="col-3">{c1.date}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
export default ShowStudentCourse;
