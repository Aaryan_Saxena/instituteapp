import React, { Component } from "react";
import http from "./httpService";
class AddCourse extends Component {
  state = {
    course: { title: "", faculty: "" },
    errors: {},
    msg: "",
    persons: [],
  };
  async componentDidMount() {
    let response = await http.get("/allPersons");
    let arr = [];
    response.data.map((obj) => {
      if (obj.role === "Faculty") {
        let json = {};
        json.id = obj.id;
        json.name = obj.name;
        return arr.push(json);
      }
    });
    this.setState({ persons: arr });
  }
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.course[input.name] = input.value;
    this.handleValidate(e);
    this.setState(s1);
  };
  isvalid = (errors) => {
    let keys = Object.keys(errors);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  isFormValid = () => {
    let errors = this.validateAll();
    return this.isvalid(errors);
  };
  validateAll = () => {
    let { title, faculty } = this.state.course;
    let errors = {};
    errors.name = this.validateName(title);
    errors.faculty = this.validateFaculty(faculty);
    return errors;
  };
  handleValidate = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    switch (input.name) {
      case "title":
        s1.errors.name = this.validateName(input.value);
        break;
      case "faculty":
        s1.errors.faculty = this.validateFaculty(input.value);
        break;
      default:
        break;
    }
    this.setState(s1);
  };
  validateName = (name) => (!name ? "Title is Required" : "");
  validateFaculty = (faculty) => (!faculty ? "Select Faculty Name" : "");

  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.course);
    this.postData("/addCourse", this.state.course);
  };
  async postData(url, obj) {
    try {
      let response = await http.post(url, obj);
      if (response) {
        alert("Course Created Succesfully");
        this.setState({
          course: { title: "", faculty: "" },
          errors: {},
        });
      }
    } catch (ex) {
      if (
        (ex.response && ex.response.status === 400) ||
        ex.response.status === 500
      ) {
        this.setState({ msg: ex.response.data });
      }
    }
  }
  render() {
    let { course, errors, roles, msg, persons = [] } = this.state;
    let { title = "", faculty = "" } = course;
    return (
      <div className="container">
        <h5 className="mt-3">Add Course</h5>
        <h5>{msg ? <span className="text-danger">{msg}</span> : ""}</h5>
        {this.makeTextField(
          "title",
          title,
          "Title",
          "Enter Course Title",
          errors.name
        )}
        {this.makeDD(
          persons,
          faculty,
          "faculty",
          "Faculty Name",
          "Select Faculty",
          errors.faculty
        )}
        <button
          className="btn btn-primary mt-2"
          disabled={!this.isFormValid()}
          onClick={this.handleSubmit}
        >
          Add
        </button>
      </div>
    );
  }
  makeDD = (arr, value, name, label, valOnTop, errors) => {
    return (
      <div className="row">
        <div className="col-12">
          <label>
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-12">
          <div className="form-group">
            <select
              className="form-control"
              name={name}
              value={value}
              onChange={this.handleChange}
              onBlur={this.handleValidate}
            >
              <option value="">{valOnTop}</option>
              {arr.map((opt) => (
                <option key={opt} value={opt.id}>
                  {opt.name}
                </option>
              ))}
            </select>
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </div>
    );
  };
  makeTextField = (name, value, label, placeholder, errors = "") => {
    let edit = this.state.edit;
    return (
      <div className="row">
        <div className="col-12">
          <label>
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-12">
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name={name}
              value={value}
              placeholder={placeholder}
              onChange={this.handleChange}
            />
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </div>
    );
  };
}
export default AddCourse;
